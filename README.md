# ABOUT

This script is for Octave and allows user to check whether selected oscillator
complies with target phase noise mask performance. It is needed, because often
times targets specify phase noise while oscillators are specified in terms of 
jitter.

Examples of this issue and calculation methodology can be found in:

- [Analog MT-008](http://www.analog.com/media/en/training-seminars/tutorials/MT-008.pdf)
- [Silabs AN256](https://www.silabs.com/documents/public/application-notes/AN256.pdf)
- *website that I started with*

Other than this calculator, there are tools available online, like:

- [Abracon](https://abracon.com/phaseNoiseCalculator.php)
- [Silab](https://www.silabs.com/tools/Pages/phase-noise-jitter-calculator.aspx)

Interesting is that for same data they produce different outputs. However, 
due to own implementation better understanding can be learned. It is important 
to note, that this tools provide examples of their products which match criteria,
but only of their own. This tool is to be more generic. Other than that, no 
online tool shows jitter contribution for frequency bins or handles phase jitter
frequency compensation (between specified mask reference frequency and target
clock frequency).

# Online results
It was said, that available online tools provide different results. 
As an example, Zynq US+ MGTH phase noise mask is:


And results are:

**Abracon:** 
```
RMS jitter = 861.260 fs, RMS Noise = 1.691e-3 rad.
See attached photo
```


**Silab:**
```
Results
The actual performance may be different and does not consider the noise floor of an actual measurement environment.
Phase Jitter:               858 fs
Period Jitter:              1.28 ps
Cycle-to-Cycle Jitters:     2.24 ps 
```
