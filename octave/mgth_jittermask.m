input_mask = [...
    0.010, -105; ...
    0.100, -124; ...
    1.000, -130; ...
    50.000, -140 ];
# ~861 fs phase jitter
reference_frequency = 312.25;
target_frequency = 156.25;    
target_frequency = reference_frequency;
 
jitter_fs = phasenoise2jitter(input_mask, target_frequency, reference_frequency);

figure(1);
semilogx(input_mask(:,1), input_mask(:,2));
ylabel('Phase noise, dBc/Hz');
xlabel('Frequency, MHz');
legend('Reference frequency (val)', 'Target frequency (val)');
title('Phase noise allowable mask vs frequency');
grid on;

figure(2);
semilogx(input_mask(:,1), jitter_fs);
ylabel('Jitter, fs');
xlabel('Frequency, MHz');
legend('Allowed phase noise jitter', 'Clock source jitter');
title('Allowable jitter vs frequency');
grid on;
