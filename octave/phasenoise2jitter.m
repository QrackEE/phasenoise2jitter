

function [jitter_rmsfsec, targetfreq_mask] = phasenoise2jitter(mask, ...
                                  target_freq = 312.5, reference_freq = 312.5)
  # nothing to do for <2 points
  if size(mask,1) < 2
    disp("ERROR! Input array must have at least 2 data pairs!");
    return;
  endif;
  
  # correct for frequency
  
  targetfreq_mask = mask;
  
  
  # integrate to obtain power noise by bin
  #  convert to reatios
  ratio = 10.^(mask(:,2)/20);
  #  integrate sub-areas
  noise_power = zeros(length(targetfreq_mask), 1);
  
  for i=1:length(targetfreq_mask)-1
    noise_power(i) = ratio(i+1) * ...
            sqrt((targetfreq_mask(i+1, 1) - targetfreq_mask(i, 1))*10^6); 

  endfor;
  
  # noise power to rms jitter (radians)
  jitter_rmsrad = sqrt(2*10.^(noise_power/10));
  #  assumption - last bin is bradband noise
  jitter_rmsrad(end) = jitter_rmsrad(end-1);
  # rms jitter (radians) to rms jitter (second)
  jitter_rmssec = jitter_rmsrad/(2*pi()*target_freq*10^6);
  # rms jitter (second) to rms jitter (femtosecond)
  jitter_rmsfsec = jitter_rmssec * 10^15;

endfunction
